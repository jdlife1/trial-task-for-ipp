<?php

Route::group(['middleware' => 'auth:api'], function () {
    Route::group(['prefix' => 'user'], function ($api) {
        $api->get('/', ['as' => 'api.auth.user', 'uses' => 'UserController@current']);
    });
    Route::group(['prefix' => 'planners'], function ($api) {
        $api->get('curate', ['as' => 'api.planners.curate', 'uses' => 'PlannerController@curate']);
        $api->post('persist', ['as' => 'api.planners.persist', 'uses' => 'PlannerController@persist']);
        Route::group(['prefix' => '{planner}/reports'], function ($api) {
            Route::group(['prefix' => 'recommendations'], function ($api) {
                $api->post('persist', ['as' => 'api.planners.reports.persist', 'uses' => 'PlannerItemRecommendationController@persist']);
            });
        });
    });
});
