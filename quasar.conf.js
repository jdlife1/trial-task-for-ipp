const path = require('path');
const webpack = require('webpack');
const envparser = require('./config/@envparser');

module.exports = function (ctx) {
  return {
    plugins: [
      'axios',
      'v-money',
      'vuex-sync',
      'router-auth',
      'aos-filters',
      'v-lazy-image',
      'vue-shortkey',
      'vue-signature',
      'vue-navigator-online',
    ],
    css: [
      'app.styl'
    ],
    extras: [
      ctx.theme.mat ? 'roboto-font' : null,
      'material-icons'
      // 'ionicons',
      // 'mdi',
      // 'fontawesome'
    ],
    supportIE: false,
    build: {
      publicPath: '/ipp-trial',
      scopeHoisting: true,
      vueRouterMode: ctx.dev ? 'history' : 'hash',
      // gzip: true,
      // analyze: true,
      // extractCSS: false,
      useNotifier: true,
      extendWebpack (cfg) {
        cfg.module.rules.push({
          enforce: 'pre',
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          exclude: /(node_modules|quasar)/,
        });

        cfg.module.rules.push({
          test: /\.pug$/,
          oneOf: [
            { exclude: /\.vue$/, use: ['pug-plain-loader'] },
            { use: ['pug-plain-loader'] }
          ]
        });

        cfg.plugins.push(
          new webpack.ProvidePlugin({
            $: 'jquery',
            jQuery: 'jquery',
            'window.jQuery': 'jquery',
            'env': 'env',
          })
        );

        cfg.optimization = {
          minimize: false,
          runtimeChunk: true,
          splitChunks: {
            chunks: "async",
            minSize: 1000,
            minChunks: 2,
            maxAsyncRequests: 5,
            maxInitialRequests: 3,
            name: true,
            cacheGroups: {
              default: {
                minChunks: 2,
                priority: -20,
                reuseExistingChunk: true,
              },
              vendors: {
                name: 'vendors',
                test: /[\\/]node_modules[\\/]/,
                priority: -10,
              },
            },
          },
        };

        cfg.resolve.alias = {
          ...cfg.resolve.alias,
          node_modules: path.resolve(__dirname, './node_modules'),
        };
      },
      env: envparser(),
    },
    devServer: {
      https: false,
      port: 9000,
      open: true // opens browser window automatically
    },
    framework: 'all', // --- includes everything; for dev only!
    // animations: 'all' --- includes all animations
    animations: [
      'fadeIn',
      'fadeOut',
    ],
    pwa: {
      workboxPluginMode: 'InjectManifest',
      workboxOptions: {
        globDirectory: './ipp',
      },
      manifest: {
        name: 'JDLife IPP - Insurance Planner and Plan',
        short_name: 'JDLife-IPP',
        description: 'JDLife Insurance Provider Planner and Plan',
        display: 'standalone',
        orientation: 'portrait',
        background_color: '#ffffff',
        theme_color: '#7a6fbe',
        icons: [
          {
            'src': 'statics/icons/icon-128x128.png',
            'sizes': '128x128',
            'type': 'image/png'
          },
          {
            'src': 'statics/icons/icon-192x192.png',
            'sizes': '192x192',
            'type': 'image/png'
          },
          {
            'src': 'statics/icons/icon-256x256.png',
            'sizes': '256x256',
            'type': 'image/png'
          },
          {
            'src': 'statics/icons/icon-384x384.png',
            'sizes': '384x384',
            'type': 'image/png'
          },
          {
            'src': 'statics/icons/icon-512x512.png',
            'sizes': '512x512',
            'type': 'image/png'
          }
        ]
      }
    },
    cordova: {
      // id: 'org.cordova.quasar.app'
    },
    electron: {
      extendWebpack (cfg) {
        // do something with cfg
      },
      packager: {
        // OS X / Mac App Store
        // appBundleId: '',
        // appCategoryType: '',
        // osxSign: '',
        // protocol: 'myapp://path',

        // Window only
        // win32metadata: { ... }
      },
      builder: {
        //
      },
    },

    // leave this here for Quasar CLI
    starterKit: '1.0.2'
  }
}
