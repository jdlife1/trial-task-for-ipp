import Vue from 'vue';
import Vuex from 'vuex';

import user from './modules/user';
import site from './modules/site';
import planner from './modules/planner';
import resources from './modules/resources';
import plannerCollection from './modules/plannerCollection';
import insuranceProvider from './modules/insuranceProvider';
import classificationUnit from './modules/classificationUnit';
import insuranceProviderReport from './modules/insuranceProviderReport';

import { persist, autosave } from './plugins';

Vue.use(Vuex);

const store = new Vuex.Store({
  modules: {
    user,
    site,
    planner,
    resources,
    plannerCollection,
    insuranceProvider,
    classificationUnit,
    insuranceProviderReport,
  },
  supportCircular: true,
  plugins: [persist.plugin, autosave],
});

export default store;
