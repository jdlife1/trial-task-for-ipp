import persist from './persist';
import autosave from './autosave';

// eslint-disable-next-line
export { persist, autosave };
