import { Notify } from 'quasar';
import { noop, every, isEmpty } from 'lodash';

const ShowMessage = (opts = {}, error = false) =>
  Notify.create({
    message: opts.message || 'Saved',
    color: error ? 'negative' : 'amber',
    icon: opts.icon || 'save',
    position: opts.position || 'top-right',
    timeout: opts.timeout || 1000,
  });

export default (store) => {
  store.subscribe((mutation, { planner, insuranceProviderReport }) => { // eslint-disable-line
    const shouldRequestAutoSave = planner.id && window.navigator.onLine;
    const { reportResults } = insuranceProviderReport;
    switch (mutation.type) {
      case 'planner/UPDATE_PLAN_FIELD_VALUE':
        if (shouldRequestAutoSave) {
          store
            .dispatch('planner/persistPlanner', planner)
            .then(payload => store.commit('plannerCollection/ADD_PLANNER_COLLECTION', payload))
            .then(ShowMessage())
            .catch(() => {
              ShowMessage({
                message: 'Auto Save Failed',
                icon: 'error',
              }, true);
            });
        }
        return;
      case 'insuranceProviderReport/UPDATE_REPORT_RESULT_DATA':
        if (shouldRequestAutoSave && !every(reportResults, isEmpty)) {
          if (reportResults.existing.length || reportResults.proposed.length) {
            store
              .dispatch('insuranceProviderReport/onRequestInsuranceReportPersist', {
                ...reportResults,
                ...{ id: planner.id, name: reportResults.name || planner.client_full_name },
              })
              .then(ShowMessage({
                message: 'Report Saved',
              }))
              .catch(() => {
                ShowMessage({
                  message: 'Auto Save Failed',
                  icon: 'error',
                }, true);
              });
          }
        }
        return;
      default: noop();
    }
  });
};
