import VuexPersistence from 'vuex-persist';

export default new VuexPersistence({
  key: '@ipp',
  storage: window.localStorage,
  modules: [
    'user',
    'planner',
    'limitedAdivce',
    'insuranceProvider',
    'clientCalculators',
    'partnerCalculators',
    'classificationUnit',
    'insuranceProviderReport',
    'insuranceProviderNoteReport',
  ],
});
