import { HTTP_API } from 'src/services/http/http';

const BASE_URL = 'https://jdserver.net/ipp/public';

export const requestInsuranceProviders = ({ commit }) => HTTP_API(`${BASE_URL}/api`).get('/insurance/providers')
  .then(({ data }) => commit('SAVE_INSURANCE_PROVIDERS', data));

export const requestProviderNoteUpdate = ({ commit }, {
  provider_id, cover_id, id, image, note, //eslint-disable-line
}) => HTTP_API().post(`/insurance/${provider_id}/cover/${cover_id}/note/${id}/update`, { //eslint-disable-line
  image,
  note,
}).then(({ data: payload }) => {
  commit('UPDATE_INSURANCE_PROVIDER_COVER_TYPE', payload.data);
  return payload;
});
