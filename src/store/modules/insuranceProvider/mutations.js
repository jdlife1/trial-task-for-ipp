import _ from 'lodash';

const SAVE_INSURANCE_PROVIDERS = (state, payload) => _.assign(state, payload);

const UPDATE_INSURANCE_PROVIDER_COVER_TYPE = (state, payload) => {
  const { insurance_provider_id, id } = payload; // eslint-disable-line

  const providerInstance = _.find(state.data, ['id', _.toNumber(insurance_provider_id)]);
  if (_.isUndefined(providerInstance)) return;

  const providerInstanceKey = _.findKey(state.data, ['id', _.toNumber(insurance_provider_id)]);

  const providerCoverTypeInstanceKey = _.findKey(providerInstance.covers, ['id', _.toNumber(id)]);
  if (_.isNaN(_.toNumber(providerCoverTypeInstanceKey))) return;

  _.set(state.data, [
    _.toNumber(providerInstanceKey),
    'covers',
    _.toNumber(providerCoverTypeInstanceKey),
  ], payload);
};

export {
  SAVE_INSURANCE_PROVIDERS,
  UPDATE_INSURANCE_PROVIDER_COVER_TYPE,
};
