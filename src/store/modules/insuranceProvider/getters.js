import { map, sortBy } from 'lodash';

export const providers = state => state.data;

export const mapInsuranceProviders = state => sortBy(map(state.data, o => ({
  label: o.name,
  value: o.name,
})), ['label']);

export const mapInsuranceProvidersById = state => sortBy(map(state.data, o => ({
  label: o.name,
  value: o.id,
})), ['label']);
