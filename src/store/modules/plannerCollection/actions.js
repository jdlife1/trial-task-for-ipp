import { HTTP_API } from 'src/services/http/http';

const requestPlannersFromApi = ({ commit }) => HTTP_API().get('/planners/curate')
  .then(({ data }) => commit('ASSIGN_PLANNER_COLLECTION', data));

const requestRemovePlanner = ({ commit }, { id }) => HTTP_API().post(`/planners/${id}/remove`)
  .then(({ data }) => commit('REMOVE_PLANNER_INSTANCE_BY_ID', data));

export {
  requestPlannersFromApi,
  requestRemovePlanner,
};
