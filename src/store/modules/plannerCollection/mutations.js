import { assign, reject, eq, toSafeInteger, head, set, filter, isEmpty, concat, findKey } from 'lodash';

const ASSIGN_PLANNER_COLLECTION = (state, payload) => assign(state, payload);

const REMOVE_PLANNER_INSTANCE_BY_ID = (state, { data: { id } }) => {
  state.data = reject(state.data, o => eq(o.id, toSafeInteger(id)));
};

const ADD_PLANNER_COLLECTION = (state, payload) => {
  payload = payload.data || payload;
  if (!isEmpty(payload)) {
    const instance = filter(state.data, ['id', payload.id]);
    if (!isEmpty(instance)) {
      set(state, ['data', toSafeInteger(findKey(state.data, ['id', head(instance).id]))], payload);
    }
    else {
      state.data = concat(state.data, payload);
    }
  }
};

export {
  ADD_PLANNER_COLLECTION,
  ASSIGN_PLANNER_COLLECTION,
  REMOVE_PLANNER_INSTANCE_BY_ID,
};
