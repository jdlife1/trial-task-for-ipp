import { toSafeInteger, find, sortBy } from 'lodash';

const planners = ({ data }) => sortBy(data, ['id']);

const getPlannerInstanceById = (state, { planners: collection }, { route: { params: { id } } }) => {
  id = toSafeInteger(id);
  if (!collection.length && !!id) return false;
  return find(collection, ['id', id]);
};

export { planners, getPlannerInstanceById };
