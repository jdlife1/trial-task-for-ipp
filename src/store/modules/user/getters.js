import { filter, isEmpty } from 'lodash';

export const currentUserModel = state => state.currentModel;

export const currentTokenDataUser = state => state.currentTokenData;

export const hasRole = ({ currentModel: { roles } }) => role =>
  !isEmpty(filter(roles, ({ name }) => name.includes(role)));

// eslint-disable-next-line no-shadow
export const isAdministrator = (state, { hasRole }) => hasRole('administrator');

// eslint-disable-next-line no-shadow
export const isAdvisor = (state, { hasRole }) => hasRole('advisor');

export const getProfileSignatureUrl = ({ currentModel: { profile } }) => profile.signature_url;
