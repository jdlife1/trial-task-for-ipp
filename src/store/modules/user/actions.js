import _ from 'lodash';
import { HTTP, HTTP_API } from 'src/services/http/http';

const requestLogin = ({ commit }, payload) => {
  const formData = _.merge(payload, {
    client_id: process.env.AUTH_CLIENT_ID,
    client_secret: process.env.AUTH_CLIENT_SECRET,
    grant_type: 'password',
  });
  return HTTP().post('/oauth/token', formData)
    .then(({ data }) => {
      commit('SET_CURRENT_USER_TOKEN', data);
      return data;
    });
};

const requestUserData = ({ commit }) => HTTP_API().get('/user')
  .then(({ data }) => commit('SET_CURRENT_USER_MODEL', data));

const requestLogout = ({ commit }) => {
  commit('REMOVE_CURRENT_USER_TOKEN');
  return commit('REMOVE_SET_CURRENT_USER_MODEL');
};

export { requestLogin, requestUserData, requestLogout };
