export const SET_CURRENT_USER_TOKEN = (state, payload) => {
  state.currentTokenData = payload;
};

export const REMOVE_CURRENT_USER_TOKEN = (state) => {
  state.currentTokenData = {};
};

export const SET_CURRENT_USER_MODEL = (state, payload) => {
  state.currentModel = payload;
};

export const REMOVE_SET_CURRENT_USER_MODEL = (state) => {
  state.currentModel = {};
};
