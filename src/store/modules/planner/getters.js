import plannerSteps from 'src/config/plannerSteps';
import { eq, toLower, map, compact, filter, trim, chain, find, isEmpty } from 'lodash';
import { floatTotals, floatFixer, numberWithCommas, switchcase } from 'src/config/utils';

/**
 * Constant deinfining liability rate store namepsace.
 *
 * @type {String}
 */
const LIABILITY_RATES_NAMESPACE = 'liabilityRates';

export const plan = state => state;

// eslint-disable-next-line
export const hasChildren = ({ have_childrens }) => eq(have_childrens, 'yes');

// eslint-disable-next-line
export const determineIncomeFromBusiness = ({ employment_position_type , income_from_business, employed_annual_income }, getters, rootState, rootGetters) => {
  // eslint-disable-next-line
  const income = floatFixer(eq(toLower(employment_position_type), 'self-employed') ? income_from_business : employed_annual_income);

  // eslint-disable-next-line
  const { employed_annual_income: incomeRequirement } = rootGetters[`${LIABILITY_RATES_NAMESPACE}/requirements`];
  const { maximum, minimum } = incomeRequirement;
  if (income > maximum) return numberWithCommas(maximum);
  if (income < minimum) return numberWithCommas(minimum);

  return numberWithCommas(income);
};

// eslint-disable-next-line
export const determineNominatedCoverAmount = ({ nominated_cover_amount: income }, getters, rootState, rootGetters) => {
  income = floatFixer(income);

  // eslint-disable-next-line
  const { nominated_cover_amount: nominatedRequirement } = rootGetters[`${LIABILITY_RATES_NAMESPACE}/requirements`];
  if (income > nominatedRequirement.maximum) return numberWithCommas(nominatedRequirement.maximum);
  if (income < nominatedRequirement.minimum) return numberWithCommas(nominatedRequirement.minimum);

  return numberWithCommas(income);
};

// eslint-disable-next-line
export const determinePartnerTakingFromTheFirm = ({ partner_taking_from_the_firm: income }, getters, rootState, rootGetters) => {
  income = floatFixer(income);
  // eslint-disable-next-line
  const { employed_annual_income: incomeRequirement } = rootGetters[`${LIABILITY_RATES_NAMESPACE}/requirements`];
  if (income > incomeRequirement.maximum) return numberWithCommas(incomeRequirement.maximum);
  if (income < incomeRequirement.minimum) return numberWithCommas(incomeRequirement.minimum);

  return numberWithCommas(income);
};

// eslint-disable-next-line
export const determinePartnerNominatedCoverAmount = ({ partner_nominated_cover_amount: income }, getters, rootState, rootGetters) => {
  income = floatFixer(income);

  // eslint-disable-next-line
  const { nominated_cover_amount: nominatedRequirement } = rootGetters[`${LIABILITY_RATES_NAMESPACE}/requirements`];
  if (income > nominatedRequirement.maximum) return numberWithCommas(nominatedRequirement.maximum);
  if (income < nominatedRequirement.minimum) return numberWithCommas(nominatedRequirement.minimum);

  return numberWithCommas(income);
};

// eslint-disable-next-line
export const hasClientIncome = ({ nominated_cover_amount }, { determineIncomeFromBusiness }) => !! floatTotals([floatFixer(determineIncomeFromBusiness), floatFixer(nominated_cover_amount)]);

// eslint-disable-next-line
export const hasPartnerIncome = ({ partner_taking_from_the_firm, partner_nominated_cover_amount }) => !! floatTotals([floatFixer(partner_taking_from_the_firm), floatFixer(partner_nominated_cover_amount)]);

export const isEmployed = state => eq(state.employment_position_type, 'employed');

export const isSelfEmployed = state => eq(state.employment_position_type, 'self-employed');

// eslint-disable-next-line
export const mapClientAndPartnerNameValues = ({ client_full_name: clientName, partner_name: partnerName }) => {
  return map(compact([clientName, partnerName]), name => ({
    label: name,
    value: name,
  }));
};

export const mapPlannerReportValues = ({ reports }) => {
  if (!reports.length) return [];
  return map(reports, ({ name, id }) => ({
    label: name,
    value: id,
  }));
};

// eslint-disable-next-line
export const mapDeterminedStepList = (state, { isEmployed, isSelfEmployed }) => {
  const filteredSteps = filter(plannerSteps, ({ employed, selfEmployed }) => {
    // eslint-disable-next-line
    if (!isEmpty(selfEmployed)) return (eq(isEmployed, employed) && eq(isSelfEmployed, selfEmployed));
    if (isEmployed) return eq(isEmployed, employed);
    return plannerSteps;
  });

  return map(filteredSteps, ({ label, value }) => ({
    label,
    value,
    color: 'secondary',
  }));
};

// eslint-disable-next-line
export const mapBusinessPartners = ({ business_partners }) => map(business_partners, ({ full_name }) => full_name);

// eslint-disable-next-line
export const getClientInsuranceProviders = ({ client_insurance_providers, client_custom_insurance_providers }) => [...client_insurance_providers, ...client_custom_insurance_providers];

// eslint-disable-next-line
export const getPartnerInsuranceProviders = ({ partner_insurance_providers, partner_custom_insurance_providers }) => [...partner_insurance_providers, ...partner_custom_insurance_providers];

export const hasPartnerExists = ({ partner_name: name }) => !!trim(name).length;

export const hasClientExists = ({ client_full_name: name }) => !!trim(name).length;

export const getChildYoungestAge = ({ fatal_entitlement_childrens: childrens }) =>
  chain(childrens).map('age').min().value() || 0;

// eslint-disable-next-line
export const getPersonsListInPLanners = ({ client_full_name: clientName, partner_name: partnerName }, { mapBusinessPartners }) => {
  return map(compact([clientName, partnerName, ...mapBusinessPartners]), name => ({
    label: name,
    value: name,
  }));
};

// eslint-disable-next-line
export const getPersonSmoker = ({ client_full_name: clientName, partner_name: partnerName, ...state }) => value => {
  if (eq(clientName, value)) {
    return {
      smoking_status: state.client_smoking_status,
    };
  }

  if (eq(partnerName, value)) {
    return {
      smoking_status: state.partner_smoking_status,
    };
  }

  return value;
};

// eslint-disable-next-line
export const getPersonsListDataInPlanners = ({ client_full_name: clientName, partner_name: partnerName, business_partners: businessPartners, ...state }, { mapBusinessPartners, determineIncomeFromBusiness, determinePartnerTakingFromTheFirm }) => query => {
  if (eq(clientName, query)) {
    return {
      income: determineIncomeFromBusiness,
      date_of_birth: state.client_date_of_birth,
      smoking_status: state.client_smoking_status,
    };
  }

  if (eq(partnerName, query)) {
    return {
      income: determinePartnerTakingFromTheFirm,
      date_of_birth: state.partner_date_of_birth,
      smoking_status: state.partner_smoking_status,
    };
  }

  return find(businessPartners, ['full_name', query]);
};

// eslint-disable-next-line
export const getCalculatedMortgageRepayment = ({ mortgage_repayments: amount, mortgage_repayments_type: frequency }) => {
  const determinePaymentByFrequency = switchcase({
    weekly: 52,
    fornightly: 26,
    monthly: 12,
    annually: 1,
  })(false);
  const payment = determinePaymentByFrequency(frequency);
  return (numberWithCommas(amount, false) * payment);
};
