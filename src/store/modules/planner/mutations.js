import { assign, set } from 'lodash';

const ASSIGN_PLANNER = (state, payload) => {
  const data = payload.data || payload;
  assign(state, data);
};

const UPDATE_PLAN_FIELD_VALUE = (state, { value, field }) => set(state, field, value);

export {
  ASSIGN_PLANNER,
  UPDATE_PLAN_FIELD_VALUE,
};
