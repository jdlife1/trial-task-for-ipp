import { cloneDeep } from 'lodash';

const businessDebtContractCategorySchema = {
  id: null,
  category_name: null,
  how_many_years: 1,
  contract_value_per_year: null,
};

const businessDebtSchema = {
  debt: null,
  personal_guarantees_in_place: 'no',
  personal_guarantee_value: null,
  any_long_term_contracts: 'no',
  contract_categories: [],
  notes: null,
};

const initialState = () => ({
  id: 0,
  reports: [],
  calculators: [],
  notes_comparisons: [],
  employment_position_type: 'self-employed',
  company_full_name: null,
  client_full_name: 'Client',
  business_partners: [], // @TODO: Relational Model
  employed_schedule_type: 'part-time',
  employed_annual_income: null,
  employed_occupation: null,
  employed_how_long: null,
  income_tax_method: 'drawings',
  on_tools: 'no',
  classification_unit: 41110,
  income_from_business: '0',
  nominated_cover_amount: '26,208',
  when_business_started: 'no',

  how_many_boys: null,
  what_you_do_in_business: null,
  has_corporate_partner: 'no',

  is_partner_shareholder_or_directory: 'yes',
  partner_name: null,
  is_partner_account_tax_splitting: 'yes',
  partner_income_tax_method: 'drawings',
  partner_taking_from_the_firm: '0',
  partner_nominated_cover_amount: '0',
  partner_on_tools: 'yes',
  partner_classification_unit: 41110,
  partner_when_business_started: null,
  partner_annual_income: null,

  partner_is_working_for_business: 'no',
  partner_is_working: 'no',
  partner_employment_position_type: 'employed',
  partner_what_job: null,

  // Business Needs
  key_persons: [],
  buy_and_sells: [],
  business_debt: cloneDeep(businessDebtSchema),

  // IMPORTANT INFO
  acc_number_business: null,
  acc_or_ird_number_personal: null,
  partner_acc_or_ird_number_personal: null,
  accountants_name: null,
  firm_name: null,
  phone_number: null, // telephone

  client_date_of_birth: (new Date()),
  client_address: null,
  client_mobile_number: null,
  client_email_address: null,
  client_smoking_status: 'no',
  client_insurance_providers: [],
  client_custom_insurance_providers: [],
  client_has_will_in_place: 'no',
  client_has_attorney: 'no',
  client_passive_income: 'no',
  client_passive_amount: null,
  client_have_been_to_doctor: 'no',
  client_have_been_to_doctor_conditions: null,
  client_have_any_hazardous_activity: 'no',
  client_hazardous_activity: null,
  client_smoked_in_last_twelve_months: 'no',
  client_citizen_or_pr: 'no',
  client_student_or_work: 'no',
  client_work_visa_greater_two_years: 'no',

  // TAX SPLITTING
  partner_date_of_birth: (new Date()),
  partner_address: null,
  partner_mobile_number: null,
  partner_email_address: null,
  partner_smoking_status: 'no',
  partner_insurance_providers: [],
  partner_custom_insurance_providers: [],
  partner_has_will_in_place: 'no',
  partner_has_attorney: 'no',
  partner_passive_income: 'no',
  partner_passive_amount: null,
  partner_have_been_to_doctor: 'no',
  partner_have_been_to_doctor_conditions: null,
  partner_have_any_hazardous_activity: 'no',
  partner_hazardous_activity: null,
  partner_smoked_in_last_twelve_months: 'no',
  partner_citizen_or_pr: 'no',
  partner_student_or_work: 'no',
  partner_work_visa_greater_two_years: 'no',

  have_childrens: 'no',
  living_expenses: null,
  household_building_expense: 'rental-payments',
  mortgage_repayments: null,
  mortgage_repayments_type: 'monthly',
  total_debts: null,
  notes: null,
  estate_notes: null,
  assets_notes: null,

  income_protection_covers_with_other_insurance_provider: null,
  passive_income_from_business: null,
  passive_income_from_rentals: null,
  other_passive_income: null,

  client_tax_issues_insurance_provider: null,
  partner_tax_issues_insurance_provider: null,

  fatal_entitlement_for_type: 'client',
  fatal_entitlement_category_type: null,
  fatal_entitlement_annual_pre_aggreed_cover_amount: null,
  fatal_entitlement_childrens: [],

  client_signature: null,
  partner_signature: null,
});

export {
  initialState,
  businessDebtSchema,
  businessDebtContractCategorySchema,
};

export default initialState;
