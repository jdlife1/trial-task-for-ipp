import { HTTP_API } from 'src/services/http/http';

export const updatePlanFieldAction = ({ commit }, payload) => commit('UPDATE_PLAN_FIELD_VALUE', payload);

export const persistPlanner = (ctx, payload) => HTTP_API().post('/planners/persist', payload)
  .then(({ data }) => data);

// eslint-disable-next-line
export const persistPlannerCalculator = (ctx, { type, field, payload, id }) => HTTP_API().post(`/planners/${id}/calculators/${type}/${field}/persist`, { payload })
  .then(({ data }) => data);

export const getPlannerReport = (ctx, { plannerId, reportId }) => HTTP_API().get(`/planners/${plannerId}/find/${reportId}`)
  .then(({ data }) => data);
