import { ItemRecommendationsTable } from 'src/components/ipp';
import { isEmpty, toString, find, map, filter, some, toNumber } from 'lodash';

const INSURANCE_PROVIDER_NAMESPACE = 'insuranceProvider';

export const reportFormData = state => state.reportFormData;

export const reportResults = state => state.reportResults;

export const currentSelectedReport = state => state.currentSelectedReport;

// eslint-disable-next-line
export const getExistingCurrentSelectedInsuranceProviderCoverTypes = ({
  reportFormData: {
    existing: { policy: { provider } },
  },
}, getters, rootState, rootGetters) => {
  if (isEmpty(toString(provider))) return [];

  const providers = rootGetters[`${INSURANCE_PROVIDER_NAMESPACE}/providers`];
  const providerInstance = find(providers, ['name', provider]);

  if (isEmpty(providerInstance)) return [];

  const { covers } = providerInstance;
  if (isEmpty(covers)) return [];

  return covers;
};

// eslint-disable-next-line
export const getProposedCurrentSelectedInsuranceProviderCoverTypes = ({
  reportFormData: {
    proposed: { policy: { provider } },
  },
}, getters, rootState, rootGetters) => {
  if (isEmpty(toString(provider))) return [];

  const providers = rootGetters[`${INSURANCE_PROVIDER_NAMESPACE}/providers`];
  const providerInstance = find(providers, ['name', provider]);
  if (isEmpty(providerInstance)) return [];

  const { covers } = providerInstance;
  if (isEmpty(covers)) return [];

  return covers;
};

// eslint-disable-next-line
export const getExistingMapCurrentSelectedInsuranceProviderCoverTypes = (state, { getExistingCurrentSelectedInsuranceProviderCoverTypes }) => {
  return map(getExistingCurrentSelectedInsuranceProviderCoverTypes, o => ({
    label: o.name,
    value: o.name,
  }));
};

// For proposed only
export const hasInsurancePolicyOnProviderResult = ({ reportResults: { proposed: proposes } }) => provider => !isEmpty(filter(proposes, ({ policies }) => some(policies, ['provider', provider])));

// eslint-disable-next-line
export const hasFidelityOnPolicyProviderResults = (state, { hasInsurancePolicyOnProviderResult }) => hasInsurancePolicyOnProviderResult('Fidelity');

// eslint-disable-next-line
export const hasPartnersLifeOnPolicyProviderResults = (state, { hasInsurancePolicyOnProviderResult }) => hasInsurancePolicyOnProviderResult('Partners Life');

// eslint-disable-next-line
export const getPropsedTotalMonthlyPremium = ({ reportResults: { proposed: proposes } }) => {
  const { methods: { getTotalMonthlyPremiumFromArray } } = ItemRecommendationsTable;
  return toNumber(getTotalMonthlyPremiumFromArray(proposes));
};

// eslint-disable-next-line
export const getProposedMapCurrentSelectedInsuranceProviderCoverTypes = (state, { getProposedCurrentSelectedInsuranceProviderCoverTypes }) => {
  return map(getProposedCurrentSelectedInsuranceProviderCoverTypes, o => ({
    label: o.name,
    value: o.name,
  }));
};

export const getReportCategories = () => map(['Apples for apples', 'Pre Underwriting', 'Post Underwriting'], category => ({
  label: category,
  value: category,
}));

export const getReportInsurancePlanTypes = () => map(['Personal', 'Business'], category => ({
  label: category,
  value: category.toLowerCase(),
}));
