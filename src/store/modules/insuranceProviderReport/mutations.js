import { set, concat } from 'lodash';

const UPDATE_REPORT_FORM_DATA = (state, { field, value }) => set(state, field, value);

const UPDATE_REPORT_RESULT_DATA = (state, { field, value }) => set(state, ['reportResults', field || null], value);

const ADD_REPORT_RESULTS_DATA = (state, payload) => {
  // check if it exists via name and offer type
  // update policies array
  state.reportResults = concat(state.reportResults, payload);
};

export {
  UPDATE_REPORT_FORM_DATA,
  ADD_REPORT_RESULTS_DATA,
  UPDATE_REPORT_RESULT_DATA,
};
