import { cloneDeep } from 'lodash';

const reportFormDataPolicySchema = {
  name: null,
  offer_type: null, // existing and proposed
  policy: {
    provider: null,
    cover_type: null,
    sum_assured: null,
    premium: null,
    notes: null,
    comments: null,
  },
};

const reportFormDataSchema = {
  id: 0,
  name: null,
  category: null,
  insurance_plan_type: null,
  planner_id: null,
  description: null,
  existing: cloneDeep(reportFormDataPolicySchema),
  proposed: cloneDeep(reportFormDataPolicySchema),
};

const reportResultSchema = {
  id: 0,
  name: null,
  category: null,
  planner_id: null,
  description: null,
  existing: [],
  proposed: [],
};

export { reportResultSchema, reportFormDataPolicySchema };

export default () => ({
  currentSelectedReport: null,
  reportFormData: cloneDeep(reportFormDataSchema),
  reportResults: cloneDeep(reportResultSchema),
});
