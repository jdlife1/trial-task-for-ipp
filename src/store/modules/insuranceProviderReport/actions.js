import { HTTP_API } from 'src/services/http/http';

const updateReportFormDataFieldAction = ({ commit }, payload) => commit('UPDATE_REPORT_FORM_DATA', payload);

const onRequestInsuranceReportPersist = (context, { id, ...payload }) => HTTP_API().post(`/planners/${id}/reports/recommendations/persist`, { ...{ id }, ...payload })
  .then(({ data }) => data);

export {
  updateReportFormDataFieldAction,
  onRequestInsuranceReportPersist,
};
