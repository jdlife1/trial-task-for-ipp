import { HTTP_API } from 'src/services/http/http';

const BASE_URL = 'https://jdserver.net/ipp/public';

// eslint-disable-next-line
export const requestClassificationUnits = ({ commit }) =>
  HTTP_API(`${BASE_URL}/api`).get('/classification/unit/curate')
    .then(({ data }) => {
      commit('SAVE_DATA', data);
      return data;
    });
