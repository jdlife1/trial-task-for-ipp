import _ from 'lodash';

export const mapClassificationUnitValues = (state) => {
  const { data } = state;
  return _.map(data, v => ({
    label: v.name,
    value: v.code,
  }));
};

export const isClasificationUnitValuesEmpty = state => _.isEmpty(state.data);
