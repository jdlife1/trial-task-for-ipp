import _ from 'lodash';

// eslint-disable-next-line
export const SAVE_DATA = (state, payload) => _.assign(state, payload);
