export default () => ({
  app: {
    config: {
      input: {
        date: {
          format: 'DD/MM/YYYY',
        },
      },
      signature: {
        format: 'image/png',
        option: {
          penColor: 'rgb(0, 0, 0)',
        },
      },
    },
    option: {
      values: {
        employmentValues: [
          {
            label: 'Employed',
            value: 'employed',
          },
          {
            label: 'Self Employed',
            value: 'self-employed',
          },
        ],
        employmentScheduleValues: [
          {
            label: 'Part Time',
            value: 'part-time',
          },
          {
            label: 'Full Time',
            value: 'full-time',
          },
        ],
        taxMethodValues: [
          {
            label: 'Drawings/Shareholder salary (i.e non paye income)',
            value: 'drawings',
          },
          {
            label: 'PAYE',
            value: 'paye',
          },
          {
            label: 'Combination of Drawings/Shareholder salary (i.e none paye income) and PAYE',
            value: 'combination-of-drawings-and-paye',
          },
        ],
        booleanValues: [
          {
            label: 'Yes',
            value: 'yes',
          },
          {
            label: 'No',
            value: 'no',
          },
        ],
        existingCovers: [
          { label: 'AIA', value: 'AIA' },
          { label: 'AMP', value: 'AMP' },
          { label: 'ANZ', value: 'ANZ' },
          { label: 'Asteron', value: 'Asteron' },
          { label: 'BNZ', value: 'BNZ' },
          { label: 'Cigna', value: 'Cigna' },
          { label: 'Fidelity', value: 'Fidelity' },
          { label: 'Kiwibank', value: 'Kiwibank' },
          { label: 'Lumley', value: 'Lumley' },
          { label: 'MAS', value: 'MAS' },
          { label: 'NIB', value: 'NIB' },
          { label: 'One Path', value: 'One Path' },
          { label: 'Partners Life', value: 'Partners Life' },
          { label: 'Pinnacle Life', value: 'Pinnacle Life' },
          { label: 'Southern Cross', value: 'Southern Cross' },
          { label: 'Sovereign', value: 'Sovereign' },
          { label: 'Vero', value: 'Vero' },
          { label: 'WestPac', value: 'WestPac' },
        ],
        clientOrPartner: [
          {
            label: 'Client',
            value: 'client',
          },
          {
            label: 'Partner',
            value: 'partner',
          },
        ],
        fatalEntitlementCategories: [
          {
            label: 'With Partner and No Child',
            value: 'no-child',
          },
          {
            label: 'With Partner and ≤ Two children younger than 18 years old',
            value: 'less-than-two-children',
          },
          {
            label: 'With Partner and > Two children younger than 18 years old',
            value: 'greater-than-two-children',
          },
          {
            label: 'With Partner and studying children above than 18 years old',
            value: 'studying-children',
          },
        ],
        mortgageRepaymentMethods: [
          {
            label: 'Weekly',
            value: 'weekly',
          },
          {
            label: 'Fornightly',
            value: 'fornightly',
          },
          {
            label: 'Monthly',
            value: 'monthly',
          },
          {
            label: 'Annually',
            value: 'annually',
          },
        ],
      },
    },
  },
});
