import { filter, includes } from 'lodash';

export const inputDateFormat = state => state.app.config.input.date.format;

export const signatureConfig = state => state.app.config.signature;

export const mortgageRepaymentMethods = state => state.app.option.values.mortgageRepaymentMethods;

export const employmentValues = state => state.app.option.values.employmentValues;

export const employmentScheduleValues = state => state.app.option.values.employmentScheduleValues;

export const taxMethodValues = state => state.app.option.values.taxMethodValues;

export const booleanValues = state => state.app.option.values.booleanValues;

export const clientOrPartner = state => state.app.option.values.clientOrPartner;

export const existingCovers = state => state.app.option.values.existingCovers;

// eslint-disable-next-line
export const fatalEntitlementCategories = state => state.app.option.values.fatalEntitlementCategories;

// eslint-disable-next-line
export const getFilteredInsuranceProvider = (state, { existingCovers }) => providers => filter(existingCovers, ({ value }) => includes(providers, value));

// eslint-disable-next-line
export const getInsuranceProvidersForTaxIssues = (state, { getFilteredInsuranceProvider }) => getFilteredInsuranceProvider(['Fidelity', 'AIA', 'Partners Life']);

// eslint-disable-next-line
export const getInsuranceProvidersForMortgageRepaymentIncomeProtection = (state, { getFilteredInsuranceProvider }) => getFilteredInsuranceProvider(['Fidelity', 'AIA', 'Partners Life', 'Asteron']);
