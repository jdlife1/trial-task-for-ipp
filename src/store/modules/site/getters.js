import { eq } from 'lodash';

export const getPlannerStep = ({ dashboard: { planner } }) => planner.step;

export const isBusinessNeedsStep = (state, { getPlannerStep: step }) =>
  eq(step, 'business-needs');
