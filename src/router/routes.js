import MasterLayout from 'layouts/Master';
import AuthIndex from 'pages/auth/Index';

import DashboardLayout from 'layouts/Dashboard';
import DashboardIndex from 'pages/dashboard/Index';
import PlannerIndex from 'pages/dashboard/planner/Index';

export default [
  {
    path: '/',
    component: MasterLayout,
    redirect: { name: 'auth' },
    children: [
      {
        path: 'auth',
        name: 'auth',
        component: AuthIndex,
      },
    ],
  },
  {
    path: '/dashboard',
    component: DashboardLayout,
    meta: { requiresAuth: true },
    children: [
      {
        path: 'app',
        name: 'dashboard.app',
        component: DashboardIndex,
        meta: { requiresAuth: true },
      },
      {
        path: 'planner/:id?',
        name: 'dashboard.planner',
        component: PlannerIndex,
        meta: { requiresAuth: true },
      },
    ],
  },
  { // Always leave this as last one
    path: '*',
    component: () => import('pages/errors/404'),
  },
];
