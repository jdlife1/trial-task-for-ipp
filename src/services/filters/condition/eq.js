function eq(arg1, arg2, arg3 = false) {
  if (!arg1) return false;
  if (arg3) return arg1 === arg2;
  return arg1 !== arg2;
}

export default eq;
