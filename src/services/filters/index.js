import _ from 'lodash';
import * as formatterFilters from './formatter';
import * as conditionFilters from './condition';

const AOSFilters = {
  install(Vue) {
    _.each(formatterFilters, (value, key) => {
      Vue.filter(key, value);
    });

    _.each(conditionFilters, (value, key) => {
      Vue.filter(key, value);
    });
  },
};

export default AOSFilters;

if (typeof window !== 'undefined' && window.Vue) {
  window.Vue.use(AOSFilters);
}
