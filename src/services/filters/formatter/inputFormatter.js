import _ from 'lodash';

function inputFormatter(value) {
  if (!_.isString(value)) return value;
  return _.replace(_.capitalize(_.trimEnd(value)), '-', ' ');
}

export default inputFormatter;
