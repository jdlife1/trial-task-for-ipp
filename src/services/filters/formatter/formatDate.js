import { date } from 'quasar';

const DATE_FORMAT = 'DD/MM/YYYY';

function formatDate(value) {
  if (!date.isValid(value)) return null;
  return date.formatDate(value, DATE_FORMAT);
}

export default formatDate;
