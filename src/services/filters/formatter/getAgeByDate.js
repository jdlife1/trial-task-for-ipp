import { date } from 'quasar';

function getAgeByDate(value, by = 'years') {
  if (!date.isValid(value)) return 0;
  value = date.getDateDiff(Date.now(), value, by);
  if (!value) return 0;
  return value;
}

export default getAgeByDate;
