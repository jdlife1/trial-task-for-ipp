export default class NoteService {
  constructor(name = null, will = 'no', attorney = 'no') {
    this._name = name; // eslint-disable-line
    this._hasWillInPlace = will; // eslint-disable-line
    this._hasAttorney = attorney; // eslint-disable-line
  }

  get name() {
    return this._name; // eslint-disable-line
  }

  get hasAttorney() {
    return this._hasAttorney === 'yes'; // eslint-disable-line
  }

  get hasWillInPlace() {
    return this._hasWillInPlace === 'yes'; // eslint-disable-line
  }

  render() {
    if (!this.name) return null;

    if (this.hasWillInPlace && this.hasAttorney) {
      return `${this.name}, you have indicated you do have a will in place. We always recommend having an up to date will in place reflecting your current wishes. You have further indicated that you do have an Enduring Power of Attorney and we recommend that this is in place with someone who you can entrust this responsibility to.`;
    }

    if (!this.hasWillInPlace && this.hasAttorney) {
      return `${this.name}, you have indicated you do not have a will in place. We always recommend having an up to date will in place reflecting your current wishes. You have further indicated that you do have an Enduring Power of Attorney and we recommend that this is in place with someone who you can entrust this responsibility to.`;
    }

    if (this.hasWillInPlace && !this.hasAttorney) {
      return `${this.name}, you have indicated you do have a will in place. We always recommend having an up to date will in place reflecting your current wishes. You have further indicated that you do not have an Enduring Power of Attorney and we recommend that this is in place with someone who you can entrust this responsibility to.`;
    }

    if (!this.hasWillInPlace && !this.hasAttorney) {
      return `${this.name}, you have indicated you do not have a will in place. We always recommend having an up to date will in place reflecting your current wishes. You have further indicated that you do not have an Enduring Power of Attorney and we recommend that this is in place with someone who you can entrust this responsibility to.`;
    }

    return null;
  }

  static getAssetsNote() {
    return 'With regard to asset disclosure, assets have not been disclosed. It has been expressed during the discovery meeting, that should something occur, your risk profile and preference is to firstly preserve the amount of wealth built up over time in the form of assets and therefore you would prefer to rely on insurance. Further noted, this reliance on insurance must fit the budgetary requirements.';
  }

  get() {
    return {
      hasAttorney: this.hasAttorney,
      hasWillInPlace: this.hasWillInPlace,
      assetsNote: this.getAssetsNote(),
      note: this.render(),
    };
  }
}
