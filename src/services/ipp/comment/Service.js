import camelCase from 'lodash/camelCase';

export default class CommentService {
  static render(type) {
    type = camelCase(type);
    if (typeof this[type] === 'function') return this[type]();
    return null;
  }

  static ruralKeyPerson() {
    return 'The sum assured benefit amount has taken into account income from your business and other sources as providing a regular payment that will cover a portion of your outgoing monthly expenses. Being able to meet your financial obligations and expenses is important to you, therefore I have recommended Rural Key person cover insurance to ensure you can meet your financial goals and maintain a quality standard of your business prior to disablement or illness.';
  }

  static keyPerson() {
    return 'The sum assured provides your business with a monthly payment based on your contribution to gross profit if you were unable to continue work due to injury or illness. The Key Person Cover can provide a monthly benefit payment to help keep your business running should you or any key employees become disabled as a result of an accident or ill health.';
  }

  static survivorsIncome() {
    return 'My recommendations for the sum assured is based on future income for your surviving partner and family, clearing of the remaining mortgage and debts, future education costs for your children and funeral costs. Family Income Protection provides for a monthly income to the family upon the life assured’s death rather than a lump sum payment. The regular monthly income assists your family to maintain the lifestyle they have before the life assured’s death.';
  }

  static familyIncome() {
    return this.survivorsIncome();
  }

  static waiverPremium() {
    return 'Waiver of Premium removes the burden of paying premiums when you are unable to work due to total disability, saving you money when you need it most and is one less thing for you to worry about.';
  }

  static realHealthCover() {
    return 'Health Cover provides greater choice and more immediate attention when you need hospital care or an operation. It is designed to cover the significant costs associated with hospitalisation and surgery.';
  }

  static totalAndPermanentDisability() {
    return 'I have included in the sum assured an amount to cover any future income you could potentially lose if you were unable to work due to disability, this amount will cover your salary for a period of time so you don’t have to worry about your day-to-day living. By having this cover in place this will provide you with the financial stability to continue to work towards your goals and life cycle objectives. Accidents do happen, and with a Total Permanent lump sum payout you’ll have resources to contribute to your home, loan or other debts, helping to safeguard your family’s future. The lump-sum payment can cover the cost of ongoing medical treatment and possible modifications to your home or work environment. Without the ability to earn, you’ll still need to pay the bills and everyday living expenses.';
  }

  static totalAndPermanentDisabilityTpd() {
    return this.totalAndPermanentDisability();
  }

  static incomeProtection() {
    return 'The sum assured benefit provides you with a monthly payment if you are unable to work due to illness or injury and with regular payments, enable you to meet your mortgage payments and have some left over to pay part of your monthly living expenses and ensure financial security. By having this cover in place this will provide you with the financial stability to continue to work towards your goals and life cycle objectives. Thus eliminating potential risks that may hinder your future plans. Being able to meet your financial obligations and expenses is important to you, therefore I have recommended Income Protection insurance to ensure you can meet your financial goals and maintain a quality standard of living prior to disablement or illness.';
  }

  static mortgageIncomeRentCover() {
    return 'Mortgage Insurance is in line with your income so if you’re off work due to sickness or injury you won’t have to worry about the stress of your monthly bills and other expenses. It is important to you that your lifestyle and goals are not affected by accident or illness and this will allow you to maximise your Income replacement should disability occur. Monthly Mortgage Repayment Cover can help cover your monthly mortgage repayments if your earning capacity is seriously affected by illness, disability or redundancy; ensuring your home stays in your and your family’s hands.';
  }

  static mortgageRepaymentCover() {
    return this.mortgageIncomeRentCover();
  }

  static monthlyMortgageRepayment() {
    return this.mortgageIncomeRentCover();
  }

  static monthlyMortgageRepaymentCover() {
    return this.mortgageIncomeRentCover();
  }

  static trauma() {
    return 'We have identified if you suffered a critical illness this recommended lump sum amount would provide substantial financial relief to you. Trauma cover will provide you with a lump sum to give you options if either of you are struck down by a serious illness, the ability to be able to take time off work without financial worries or a recuperating holiday or help with medical costs are things these funds can be used for. Trauma Cover gives you financial freedom to make your own choices, at a time when your health has been seriously affected, and you may have to rethink your future.';
  }

  static traumaMulti() {
    return this.trauma();
  }

  static progressiveCare() {
    return this.trauma();
  }

  static comprehensiveLiving() {
    return this.trauma();
  }

  static essentialLiving() {
    return this.trauma();
  }

  static essentialLivingAssurance() {
    return this.trauma();
  }

  static comprehensiveLivingAssurance() {
    return this.trauma();
  }

  static lifeCover() {
    return 'Life Cover will ensure that in the event of your untimely death a lump sum payment will be available. This level of cover currently meets your financial commitments, leaving no debts, liabilities or burdens on family and friends in the event of your untimely death. The Life cover recommended will enable a surviving family to lead the life you want without financial restraint. Life Cover protects the lifestyle and future of your family and loved ones if you die. You’ve indicated it is important to you that your family are able to cope with paying the monthly bills without your salary. Life Cover is designed to pay a lump sum if you die prematurely.';
  }
}
