import { find, get } from 'lodash';

export default class CalculatorService {
  constructor(planner = {}, type = 'client') {
    this.planner = planner;
    this.type = type;
  }

  getCalculatorFromPlanner() {
    const { calculators } = this.planner;
    return find(calculators, ['type', this.type]) || null;
  }

  getCalculatorFields(field) {
    return get(this.getCalculatorFromPlanner(), field, null);
  }

  getLifeCover() {
    return this.getCalculatorFields('life_cover');
  }

  getMortgageRepaymentAndIncomeProtection() {
    return this.getCalculatorFields('mortgage_repayment_and_income_protection');
  }

  getMortgageRepaymentIncomeProtectionAndHec() {
    return this.getCalculatorFields('mortgage_repayment_income_protection_and_hec');
  }

  getTotalAndPermanentDisablement() {
    return this.getCalculatorFields('total_and_permanent_disablement');
  }

  getTraumaCover() {
    return this.getCalculatorFields('trauma_cover');
  }

  getTpd() {
    return this.getCalculatorFields('tpd');
  }

  getAll() {
    return {
      getTpd: this.getTpd(),
      getLifeCover: this.getLifeCover(),
      getMortgageRepaymentAndIncomeProtection: this.getMortgageRepaymentAndIncomeProtection(),
      getMortgageRepaymentIncomeProtectionAndHec: this.getMortgageRepaymentIncomeProtectionAndHec(),
      getTotalAndPermanentDisablement: this.getTotalAndPermanentDisablement(),
      getTraumaCover: this.getTraumaCover(),
    };
  }
}
