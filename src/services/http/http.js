import axios from 'axios';
import store from 'src/store';
import { Notify } from 'quasar';
import router from 'src/router';
import { map, each, forOwn } from 'lodash';

const HTTP_API = (baseURL = null, opts = {}) => {
  const { access_token } = store.getters['user/currentTokenDataUser']; // eslint-disable-line

  const httpInstance = axios.create({
    baseURL: baseURL || `${process.env.WEB_ENDPOINT}/api`,
    headers: {
      Authorization: `Bearer ${access_token}`, // eslint-disable-line
    },
    ...opts,
  });

  httpInstance.interceptors.response.use(response => response, (error) => {
    if (error.response.status === 401) {
      Notify.create({
        message: 'Unauthorized',
        color: 'negative',
        icon: 'warning',
      });
      store.commit('user/REMOVE_CURRENT_USER_TOKEN');
      store.commit('user/REMOVE_SET_CURRENT_USER_MODEL');
      router.replace('/auth');
    }
    else if (error.response.status === 422) {
      if (error.response.data.errors) {
        const errorMessages = [];
        map(
          error.response.data.errors,
          obj => forOwn(
            obj,
            value => errorMessages.push(value),
          ),
        );
        each(errorMessages, (v) => {
          Notify.create({
            message: v,
            color: 'negative',
            icon: 'warning',
          });
        });
      }
      else {
        Notify.create({
          message: error.response.data.message,
          color: 'negative',
          icon: 'warning',
        });
      }
    }
    else if (error.response.status === 500 || error.response.status === 501) {
      Notify.create({
        message: 'Server Error',
        color: 'negative',
        icon: 'warning',
      });
    }
    return Promise.reject(error.response);
  });

  return httpInstance;
};

const HTTP = () => {
  const httpInstance = axios.create({
    baseURL: process.env.WEB_ENDPOINT,
  });

  httpInstance.interceptors.response.use(response => response, (error) => {
    if (error.response.status === 401) {
      Notify.create({
        message: 'Unauthorized',
        color: 'negative',
        icon: 'warning',
      });
    }
    return Promise.reject(error.response);
  });

  return httpInstance;
};

export {
  HTTP,
  HTTP_API,
};
