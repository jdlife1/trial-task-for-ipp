/* eslint-disable */
import _ from 'lodash'
import { numberWithCommas } from 'src/config/utils'
import { req, withParams } from 'vuelidate/lib/validators/common'

export default (max) => withParams(
  { type: 'maxValue', max }, value => {
    if (_.isString(value)) {
      value = numberWithCommas(value, false)
    }
    return !req(value) || ((!/\s/.test(value) || value instanceof Date) && +value <= +max)
  }
)
