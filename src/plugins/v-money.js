import money from 'src/components/vmoney';

export default ({ Vue }) => {
  Vue.use(money, {
    precision: 0,
  });
};
