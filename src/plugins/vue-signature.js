import vueSignature from 'vue-signature';

export default ({ Vue }) => {
  Vue.use(vueSignature);
};
