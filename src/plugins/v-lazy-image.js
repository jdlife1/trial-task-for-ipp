import { VLazyImagePlugin } from 'v-lazy-image';

export default ({ Vue }) => {
  Vue.use(VLazyImagePlugin);
};
