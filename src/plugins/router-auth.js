import _ from 'lodash';
import store from 'src/store';

export default ({ router }) => {
  router.beforeEach((to, from, next) => {
    if (to.matched.some(record => record.meta.requiresAuth)) {
      const userTokenData = store.getters['user/currentTokenDataUser'];
      const userModel = store.getters['user/currentUserModel'];
      if (_.isEmpty(userTokenData) && _.isEmpty(userModel)) {
        next({
          path: '/auth',
          query: {
            redirect: to.fullPath,
          },
        });
      }
      else {
        next();
      }
    }
    else {
      next();
    }
  });
};
