import AOSFilters from 'src/services/filters';

export default ({ Vue }) => {
  Vue.prototype.$filters = Vue.options.filters;

  Vue.use(AOSFilters);
};
