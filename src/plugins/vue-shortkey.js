const ShortKey = require('vue-shortkey');

export default ({ Vue }) => {
  Vue.use(ShortKey);
};
