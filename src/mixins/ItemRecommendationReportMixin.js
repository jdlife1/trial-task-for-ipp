import _ from 'lodash';
import { openURL } from 'quasar';
import { mapGetters, mapMutations } from 'vuex';
import { ItemRecommendationsTable } from 'src/components/ipp';
import CommentService from 'src/services/ipp/comment/Service';
import { reportFormDataPolicySchema } from 'src/store/modules/insuranceProviderReport/state';

export default {
  data: () => ({
    arrayOfExistingPolicyProviders: [],
    arrayOfProposedPolicyProviders: [],
    forceAdd: {
      existing: false,
      proposed: false,
    },
    selectedReportModel: null,
  }),
  watch: {
    forceAdd: {
      handler({ existing, proposed }) {
        if (!existing && !proposed) return; // eslint-disable-line
        if (existing && proposed) {
          this.forceAdd = {
            existing: false,
            proposed: false,
          };
        }
      },
      deep: true,
    },
    arrayOfExistingPolicyProviders: {
      handler: 'arrayOfExistingPolicyProvidersHandler',
      deep: true,
    },
    arrayOfProposedPolicyProviders: {
      handler: 'arrayOfProposedPolicyProvidersHandler',
      deep: true,
    },
    reportFormData: {
      handler: 'reportFormDataHandler',
      deep: true,
    },
  },
  methods: {
    ...mapMutations('insuranceProviderReport', ['UPDATE_REPORT_FORM_DATA', 'UPDATE_REPORT_RESULT_DATA']),
    arrayOfProposedPolicyProvidersHandler(values) {
      _.each(values, ({ name, policies }) => {
        if (_.isEmpty(policies)) {
          this.arrayOfProposedPolicyProviders.splice(_.findIndex(this.arrayOfProposedPolicyProviders, ['name', name]), 1);
        }
      });
      this.UPDATE_REPORT_RESULT_DATA({
        value: values,
        field: 'proposed',
      });
    },
    arrayOfExistingPolicyProvidersHandler(values) {
      _.each(values, ({ name, policies }) => {
        if (_.isEmpty(policies)) {
          this.arrayOfExistingPolicyProviders.splice(_.findIndex(this.arrayOfExistingPolicyProviders, ['name', name]), 1);
        }
      });
      this.UPDATE_REPORT_RESULT_DATA({
        value: values,
        field: 'existing',
      });
    },
    currentSelectedReportHandler(payload) {
      const { existing, proposed, id } = payload;
      this.updateInsuranceProviderField(id, ['currentSelectedReport']);
      this.updateInsuranceProviderField(payload, 'reportResults');
      this.arrayOfExistingPolicyProviders = existing;
      this.arrayOfProposedPolicyProviders = proposed;
      this.$nextTick(() => this.renderTableSameHeight());
    },
    reportFormDataHandler({
      existing: {
        policy: {
          provider: existingProvider,
          notes: existingNotes,
          cover_type: existingCoverType,
        },
      },
      proposed: {
        policy: {
          provider: proposedProvider,
          notes: proposedNotes,
          cover_type: proposedCoverType,
        },
      },
    }) {
      if (!_.isEmpty(existingProvider) && _.eq(existingProvider, 'First Appointment')) {
        if (_.isEmpty(existingNotes)) {
          this.updateInsuranceProviderField(this.covertArrToNewLine(['Existing Policy Schedule is being attained.']), ['reportFormData', 'existing', 'policy', 'notes']);
        }
      }

      if (!_.isEmpty(proposedProvider) && _.eq(proposedProvider, 'First Meeting Report')) {
        if (_.isEmpty(proposedNotes)) {
          this.updateInsuranceProviderField(this.covertArrToNewLine(['Due diligence shall be completed to explore best recommended insurance provider', 'Recommendations shall be prepared for the next meeting']), ['reportFormData', 'proposed', 'policy', 'notes']);
        }
      }

      if (!_.isEmpty(_.toString(existingCoverType))) {
        const existingPolicyInstance = _.find(this.getExistingCurrentSelectedInsuranceProviderCoverTypes, ['name', _.toString(existingCoverType)]);
        if (!_.isEmpty(existingPolicyInstance)) {
          const { notes: existingPolicyInstanceNotes } = existingPolicyInstance;
          if (_.isEmpty(existingNotes) && !_.isEmpty(existingPolicyInstanceNotes)) {
            this.updateInsuranceProviderField(this.covertArrToNewLine(existingPolicyInstanceNotes), ['reportFormData', 'existing', 'policy', 'notes']);
          }
        }
      }

      if (!_.isEmpty(_.toString(proposedCoverType))) {
        const proposedPolicyInstance = _.find(this.getProposedCurrentSelectedInsuranceProviderCoverTypes, ['name', _.toString(proposedCoverType)]);
        if (!_.isEmpty(proposedPolicyInstance)) {
          const { notes: proposedPolicyInstanceNotes } = proposedPolicyInstance;
          if (_.isEmpty(proposedNotes) && !_.isEmpty(proposedPolicyInstanceNotes)) {
            this.updateInsuranceProviderField(this.covertArrToNewLine(proposedPolicyInstanceNotes), ['reportFormData', 'proposed', 'policy', 'notes']);
          }
        }
      }
    },
    addArrayOfProposedPolicyProviders(ev, clientAndPolicyObject = null) {
      // eslint-disable-next-line
      let { proposed: { name, policy } } = clientAndPolicyObject ? clientAndPolicyObject : this.reportFormData;
      policy.comments = CommentService.render(policy.cover_type);
      policy = this.convertToNonReactive(policy);

      const proposed = {
        name,
        offer_type: 'proposed',
        policies: [],
      };
      let proposedInstance = _.find(this.arrayOfProposedPolicyProviders, ['name', name]);
      if (_.isEmpty(proposedInstance)) {
        proposed.policies = _.castArray(policy);
        proposedInstance = _.cloneDeep(proposed);
        this.arrayOfProposedPolicyProviders.push(proposedInstance);
      }
      else {
        proposedInstance.policies.push(policy);
      }

      // Check if force add
      if (this.forceAdd.proposed) {
        this.addArrayOfExistingPolicyProviders(ev, {
          existing: {
            name,
            policy: {
              cover_type: null,
              notes: null,
              provider: null,
              sum_assured: null,
              premium: null,
            },
          },
        });
      }
      // Reset propsed fields
      this.updateInsuranceProviderField(_.cloneDeep(reportFormDataPolicySchema), ['reportFormData', 'proposed']);
      this.$nextTick(() => this.renderTableSameHeight());
    },
    removePoliciesFromArrayOfProposedPolicyProviders(proposedProvidersIndex, policyIndex) {
      this.arrayOfProposedPolicyProviders[proposedProvidersIndex].policies.splice(policyIndex, 1);
    },
    addArrayOfExistingPolicyProviders(ev, clientAndPolicyObject = null) {
      // eslint-disable-next-line
      let { existing: { name, policy } } = clientAndPolicyObject ? clientAndPolicyObject : this.reportFormData;
      policy.comments = CommentService.render(policy.cover_type);
      policy = this.convertToNonReactive(policy);

      const existing = {
        name,
        offer_type: 'existing',
        policies: [],
      };
      let exisitngInstance = _.find(this.arrayOfExistingPolicyProviders, ['name', name]);

      if (_.isEmpty(exisitngInstance)) {
        existing.policies = _.castArray(policy);
        exisitngInstance = _.cloneDeep(existing);
        this.arrayOfExistingPolicyProviders.push(exisitngInstance);
      }
      else {
        exisitngInstance.policies.push(_.cloneDeep(policy));
      }

      // Check if force add
      if (this.forceAdd.existing) {
        this.addArrayOfProposedPolicyProviders(ev, {
          proposed: {
            name,
            policy: {
              cover_type: null,
              notes: null,
              provider: null,
              sum_assured: null,
              premium: null,
            },
          },
        });
      }
      // Reset existing fields
      this.updateInsuranceProviderField(_.cloneDeep(reportFormDataPolicySchema), ['reportFormData', 'existing']);
      this.$nextTick(() => this.renderTableSameHeight());
    },
    removePoliciesFromArrayOfExistingPolicyProviders(existingProvidersIndex, policyIndex) {
      this.arrayOfExistingPolicyProviders[existingProvidersIndex].policies.splice(policyIndex, 1);
    },
    previewAsPDF(e, name = 'document.recommendation.preview') {
      const { insurance_plan_type: type } = this.reportResults;
      const { href } = this.$router.resolve({
        name,
        query: {
          type,
        },
      });
      openURL(href);
    },
    covertArrToNewLine(arrText) {
      return _.join(arrText, '\n\n');
    },
    clearTableFields(e, fields = ['arrayOfExistingPolicyProviders', 'arrayOfProposedPolicyProviders']) {
      this.$q.dialog({
        title: 'Warning',
        message: 'Are you sure you wanted to clear all data',
        ok: 'Clear',
        cancel: 'Cancel',
      }).then(() => _.each(fields, model => this[model] = [])); //eslint-disable-line
    },
    updateInsuranceProviderField(value, field) {
      this.UPDATE_REPORT_FORM_DATA({ value, field });
    },
  },
  computed: {
    ...mapGetters('planner', {
      mapPlannerReportValues: 'mapPlannerReportValues',
    }),
    ...mapGetters('insuranceProvider', {
      insuranceProviders: 'providers',
      mapInsuranceProviders: 'mapInsuranceProviders',
    }),
    ...mapGetters('insuranceProviderReport', {
      currentSelectedReport: 'currentSelectedReport',
      reportResults: 'reportResults',
      reportFormData: 'reportFormData',
      getExistingCurrentSelectedInsuranceProviderCoverTypes: 'getExistingCurrentSelectedInsuranceProviderCoverTypes',
      getProposedCurrentSelectedInsuranceProviderCoverTypes: 'getProposedCurrentSelectedInsuranceProviderCoverTypes',
      getExistingMapCurrentSelectedInsuranceProviderCoverTypes: 'getExistingMapCurrentSelectedInsuranceProviderCoverTypes',
      getProposedMapCurrentSelectedInsuranceProviderCoverTypes: 'getProposedMapCurrentSelectedInsuranceProviderCoverTypes',
    }),
  },
  components: {
    ItemRecommendationsTable,
  },
};
