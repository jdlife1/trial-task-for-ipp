import { numberWithCommas } from 'src/config/utils';
import { isArray, clone, lowerCase, toString } from 'lodash';
import { mapGetters, mapActions, mapMutations } from 'vuex';

export default {
  methods: {
    ...mapMutations('planner', ['ASSIGN_PLANNER']),
    ...mapMutations('plannerCollection', ['ADD_PLANNER_COLLECTION']),
    ...mapActions('planner', ['updatePlanFieldAction', 'persistPlanner']),
    async onHandlePersist() {
      try {
        this.$q.notify({
          message: 'Saving',
          color: 'primary',
          icon: 'save',
          timeout: 1000,
          position: 'top-right',
        });
        const { data: payload } = await this.persistPlanner(this.plan);
        await this.ASSIGN_PLANNER(payload);
        await this.ADD_PLANNER_COLLECTION(payload);
        this.$q.notify({
          message: 'Saved',
          color: 'secondary',
          icon: 'check',
          timeout: 1000,
          position: 'top-right',
        });
        await this.$router.replace({
          name: 'dashboard.planner',
          params: {
            id: payload.id,
          },
        });
      }
      catch (e) {} // eslint-disable-line
    },
    updatePlanField(value, field) {
      if (isArray(value)) value = clone(value);
      return this.updatePlanFieldAction({ value, field });
    },
    onAmountBlurHandler(field) {
      const { minimum, maximum } = this.liabilityRequirement[field]; // eslint-disable-line
      const { minValue, maxValue } = this.$v.plan[field]; // eslint-disable-line
      // if (!minValue) this.updatePlanField(numberWithCommas(minimum), field);
      // if (!maxValue) this.updatePlanField(numberWithCommas(maximum), field);
    },
    getAmountFieldMessage(field) {
      const { minimum, maximum } = this.liabilityRequirement[field];
      const { minValue, maxValue, required } = this.$v.plan[field];

      if (!minValue) {
        // eslint-disable-next-line
        return `The amount you have entered is less than the minimum Liable earning of $${numberWithCommas(minimum)} this amount will be used in your calculations.`;
      }

      if (!maxValue) {
        // eslint-disable-next-line
        return `The amount you have entered is more than the maximum Liable earning of $${numberWithCommas(maximum)} this amount will be used in your calculations.`;
      }

      if (!required) {
        return 'This field is required.';
      }

      return null;
    },
    convertToNonReactive(payload) {
      return JSON.parse(JSON.stringify(payload));
    },
    filterByValueFn(terms, obj) {
      // eslint-disable-next-line
      return lowerCase(toString(obj.value)).indexOf(toString(terms)) > -1
        || lowerCase(obj.label).indexOf(lowerCase(terms)) > -1;
    },
  },
  computed: {
    ...mapGetters('planner', {
      plan: 'plan',
      hasClientExists: 'hasClientExists',
      hasPartnerExists: 'hasPartnerExists',
    }),
    ...mapGetters('liabilityRates', {
      liabilityRequirement: 'requirements',
    }),
    getClientName() {
      return this.hasClientExists ? this.plan.client_full_name : 'Client';
    },
    getPartnerName() {
      return this.hasPartnerExists ? this.plan.partner_name : 'Partner';
    },
  },
};
