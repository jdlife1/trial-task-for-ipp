import FieldableMixin from './FieldableMixin';
import ItemRecommendationTableMixin from './ItemRecommendationTableMixin';
import ItemRecommendationReportMixin from './ItemRecommendationReportMixin';

export {
  FieldableMixin,
  ItemRecommendationTableMixin,
  ItemRecommendationReportMixin,
};
