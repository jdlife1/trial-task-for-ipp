import jQuery from 'jquery';
import max from 'lodash/max';

export default {
  methods: {
    renderTableSameHeight(
      includeMargin = true,
      tableFirstSelector = '#item-recommendation-table-existing',
      tableSecondSelector = '#item-recommendation-table-proposed',
      tableBodySelector = '.item__table__body',
    ) {
      const $firstTableSelector = jQuery(`${tableSecondSelector} ${tableBodySelector}`);
      $firstTableSelector.each(function (index, element) { //eslint-disable-line
        const rowOneHeight = jQuery(this).outerHeight(includeMargin);
        const rowTwo = jQuery(`${tableFirstSelector} ${tableBodySelector}:eq('${index}')`);
        if (!rowTwo.length || rowTwo.hasClass('bottom-less-row')) return false;
        const rowTwoHeight = rowTwo.outerHeight(includeMargin);
        const totalHeight = max([rowOneHeight, rowTwoHeight]);
        rowTwo.css('height', totalHeight);
        jQuery(this).css('height', totalHeight);
      });
    },
  },
};
