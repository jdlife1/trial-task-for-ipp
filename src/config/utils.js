/* eslint-disable */
import _ from 'lodash';

const floatFixer = (x, precision = 2, comma = false) => _.toNumber(window.parseFloat(numberWithCommas(x, comma)).toFixed(precision));

const floatTotals = (x, y = 0, precision = 2) => {
  if (_.isArray(x)) {
    return _.reduce(x, (sum, n) => sum + n, 0);
  }
  return floatFixer(window.parseFloat(x) + window.parseFloat(y), precision);
};

const floatDiff = (x, y, precision = 2) => (floatFixer(floatFixer(x) - floatFixer(y), precision));

const numberWithCommas = (x, comma = true) => {
  if (!x) return _.toNumber(0);
  if (comma) return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',');
  return _.toNumber(x.toString().split(',').join(''));
};

const floatTaxTotal = (x, y, precision = 2, percent = 100) => floatFixer(floatFixer(x, precision) * (y / percent), precision);

const getPercentageValue = (x, y, p = 100) => Math.round(((x - y) / x * p));

const switchcase = cases => defaultCase => key =>
  cases.hasOwnProperty(key) ? cases[key] : defaultCase;

export {
  floatDiff,
  switchcase,
  floatFixer,
  floatTotals,
  floatTaxTotal,
  numberWithCommas,
  getPercentageValue,
};
