export const clientCalculatorOptions = [
  {
    label: 'TPD',
    value: 'client-tpd',
    sublabel: 'Client',
  },
  {
    label: 'Life Cover',
    value: 'client-life-cover',
    sublabel: 'Client',
  },
  {
    label: 'Trauma Cover',
    value: 'client-trauma-cover',
    sublabel: 'Client',
  },
  {
    label: 'Total and Permanent Disablement',
    value: 'client-total-and-permanent-disablement',
    sublabel: 'Client',
  },
  {
    label: 'Income Protection',
    value: 'client-income-protection',
    sublabel: 'Client',
  },
  {
    label: 'Mortgage Repayment and Income Protection',
    value: 'client-mortgage-repayment-and-income-protection',
    sublabel: 'Client',
  },
  {
    label: 'Mortgage Repayment, Income Protection and House Hold Expense Cover',
    value: 'client-mortgage-repayment-income-protection-and-house-expense-cover',
    sublabel: 'Client',
  },
  {
    label: 'Tax Calculator',
    value: 'client-tax-calculator',
    sublabel: 'Client',
  },
];

export const partnerCalculatorOptions = [
  {
    label: 'TPD',
    value: 'partner-tpd',
    sublabel: 'Partner',
  },
  {
    label: 'Life Cover',
    value: 'partner-life-cover',
    sublabel: 'Partner',
  },
  {
    label: 'Trauma Cover',
    value: 'partner-trauma-cover',
    sublabel: 'Partner',
  },
  {
    label: 'Total and Permanent Disablement',
    value: 'partner-total-and-permanent-disablement',
    sublabel: 'Partner',
  },
  {
    label: 'Income Protection',
    value: 'partner-income-protection',
    sublabel: 'Partner',
  },
  {
    label: 'Mortgage Repayment and Income Protection',
    value: 'partner-mortgage-repayment-and-income-protection',
    sublabel: 'Partner',
  },
  {
    label: 'Mortgage Repayment, Income Protection and House Hold Expense Cover',
    value: 'partner-mortgage-repayment-income-protection-and-house-expense-cover',
    sublabel: 'Partner',
  },
  {
    label: 'Tax Calculator',
    value: 'partner-tax-calculator',
    sublabel: 'Partner',
  },
];

export const commonDocumentSections = [
  {
    label: 'Item Recommendations',
    value: 'item-recommendations',
  },
  {
    label: 'ACC Savings',
    value: 'acc-savings',
  },
  {
    label: 'ACC Offsets',
    value: 'acc-offsets',
  },
  {
    label: 'Passive Income',
    value: 'passive-income',
  },
  {
    label: 'Fatal Entitlements',
    value: 'fatal-entitlements',
  },
  {
    label: 'Tax Issues',
    value: 'tax-issues',
  },
];

export default [
  ...commonDocumentSections,
  ...clientCalculatorOptions,
  ...partnerCalculatorOptions,
];
