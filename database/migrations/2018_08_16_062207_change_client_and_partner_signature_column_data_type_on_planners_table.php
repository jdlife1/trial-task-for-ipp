<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeClientAndPartnerSignatureColumnDataTypeOnPlannersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('planners', function (Blueprint $table) {
            DB::statement('ALTER TABLE planners MODIFY client_signature  LONGTEXT;');
            DB::statement('ALTER TABLE planners MODIFY partner_signature  LONGTEXT;');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('planners', function (Blueprint $table) {
            DB::statement('ALTER TABLE planners MODIFY client_signature TEXT;');
            DB::statement('ALTER TABLE planners MODIFY partner_signature TEXT;');
        });
    }
}
