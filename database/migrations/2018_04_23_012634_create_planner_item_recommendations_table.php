<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePlannerItemRecommendationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('planner_item_recommendations', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('planner_id');
            $table->foreign('planner_id')
                    ->references('id')
                    ->on('planners')
                    ->onDelete('CASCADE');
            $table->string('name');
            $table->text('existing')->nullable(true);
            $table->text('proposed')->nullable(true);
            $table->string('description')->nullable(true);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('planner_item_recommendations');
    }
}
