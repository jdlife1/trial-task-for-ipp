<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddClientAndPartnerImportantInformationFields extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('planners', function (Blueprint $table) {
            $table
                ->string('client_has_will_in_place')
                ->after('client_insurance_providers')
                ->nullable(true);
            $table
                ->string('client_has_attorney')
                ->after('client_has_will_in_place')
                ->nullable(true);

            $table
                ->string('partner_has_will_in_place')
                ->after('partner_insurance_providers')
                ->nullable(true);
            $table
                ->string('partner_has_attorney')
                ->after('partner_has_will_in_place')
                ->nullable(true);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('planner', function (Blueprint $table) {
            $table->dropColumn([
                'client_has_will_in_place',
                'client_has_will_in_place',
                'partner_has_will_in_place',
                'partner_has_attorney',
            ]);
        });
    }
}
