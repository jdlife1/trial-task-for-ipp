<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTaxIssuesInsuranceProviderColumnOnPlannersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('planners', function (Blueprint $table) {
            $table
                ->string('client_tax_issues_insurance_provider')
                ->after('other_passive_income')
                ->nullable(true);
            $table
                ->string('partner_tax_issues_insurance_provider')
                ->after('client_tax_issues_insurance_provider')
                ->nullable(true);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('planners', function (Blueprint $table) {
            $table->dropColumn([
                'client_tax_issues_insurance_provider',
                'partner_tax_issues_insurance_provider',
            ]);
        });
    }
}
