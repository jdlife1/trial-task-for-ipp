<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPartnerFurtherOccupationQuestionColumnsOnPlannersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('planners', function (Blueprint $table) {
            $table
                ->string('partner_is_working_for_business')
                ->nullable(true)
                ->default('no')
                ->after('partner_when_business_started');
            $table
                ->string('partner_is_working')
                ->nullable(true)
                ->default('no')
                ->after('partner_is_working_for_business');
            $table
                ->string('partner_employment_position_type')
                ->nullable(true)
                ->default('employed')
                ->after('partner_is_working');
            $table
                ->string('partner_what_job')
                ->nullable(true)
                ->after('partner_employment_position_type');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('planners', function (Blueprint $table) {
            $table->dropColumn([
                'partner_is_working_for_business',
                'partner_is_working',
                'partner_employment_position_type',
                'partner_what_job',
            ]);
        });
    }
}
