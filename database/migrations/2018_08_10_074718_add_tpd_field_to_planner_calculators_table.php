<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTpdFieldToPlannerCalculatorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('planner_calculators', function (Blueprint $table) {
            $table
                ->text('tpd')
                ->after('type')
                ->nullable(true);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('planner_calculators', function (Blueprint $table) {
            $table->dropColumn('tpd');
        });
    }
}
