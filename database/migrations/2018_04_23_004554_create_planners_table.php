<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePlannersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('planners', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('user_id');
            $table->foreign('user_id')
                    ->references('id')
                    ->on('users')
                    ->onDelete('CASCADE');

            foreach ($this->stringFields() as $field) {
                $table->string($field)->nullable(true);
            }

            foreach (array_merge($this->jsonFields(), $this->textFields()) as $field) {
                $table->text($field)->nullable(true);
            }

            foreach ($this->dateFields() as $field) {
                $table->timestamp($field)->default(null)->nullable(true);
            }

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('planners');
    }

    protected function stringFields()
    {
        return [
            'employment_position_type',
            'employed_schedule_type',
            'employed_annual_income',
            'employed_occupation',
            'employed_how_long',
            'company_full_name',
            'client_full_name',
            'income_tax_method',
            'on_tools',
            'has_corporate_partner',
            'classification_unit',
            'income_from_business',
            'nominated_cover_amount',
            'how_many_boys',
            'what_you_do_in_business',
            'acc_number_business',
            'acc_or_ird_number_personal',
            'partner_acc_or_ird_number_personal',
            'accountants_name',
            'firm_name',
            'phone_number',
            'client_address',
            'client_mobile_number',
            'client_email_address',
            'client_smoking_status',
            'partner_address',
            'partner_mobile_number',
            'partner_email_address',
            'partner_smoking_status',
            'have_childrens',
            'living_expenses',
            'mortgage_repayments',
            'mortgage_repayments_type',
            'total_debts',
            'is_partner_shareholder_or_directory',
            'partner_name',
            'is_partner_account_tax_splitting',
            'partner_income_tax_method',
            'partner_taking_from_the_firm',
            'partner_on_tools',
            'partner_classification_unit',
            'partner_nominated_cover_amount',
            'income_protection_covers_with_other_insurance_provider',
            'passive_income_from_business',
            'passive_income_from_rentals',
            'other_passive_income',
            'fatal_entitlement_for_type',
            'fatal_entitlement_category_type',
            'fatal_entitlement_annual_pre_aggreed_cover_amount',
        ];
    }

    protected function textFields()
    {
        return [
            'notes',
            'client_signature',
            'partner_signature',
        ];
    }

    protected function dateFields()
    {
        return [
            'when_business_started',
            'client_date_of_birth',
            'partner_when_business_started',
            'partner_date_of_birth',
        ];
    }

    protected function jsonFields()
    {
        return [
            'business_partners',
            'fatal_entitlement_childrens',
            'client_insurance_providers',
            'partner_insurance_providers',
        ];
    }
}
