<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeDateFieldsTypeToString extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('planners', function (Blueprint $table) {
            foreach ($fields = $this->dateFields() as $index => $field) {
                if ($index == 0) {
                    $table
                        ->string($field)
                        ->after('fatal_entitlement_annual_pre_aggreed_cover_amount')
                        ->nullable(true)
                        ->change();
                }
                else {
                    $table
                        ->string($field)
                        ->after($fields[$index - 1])
                        ->nullable(true)
                        ->change();
                }
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('planners', function (Blueprint $table) {
            $table->dropColumn($this->dateFields());
        });

        Schema::table('planners', function (Blueprint $table) {
            foreach ($fields = $this->dateFields() as $index => $field) {
                if ($index == 0) {
                    $table
                        ->timestamp($field)
                        ->after('fatal_entitlement_annual_pre_aggreed_cover_amount')
                        ->nullable(true);
                }
                else {
                    $table
                        ->timestamp($field)
                        ->after($fields[$index - 1])
                        ->nullable(true);
                }
            }
        });
    }

    protected function dateFields()
    {
        return [
            'when_business_started',
            'client_date_of_birth',
            'partner_when_business_started',
            'partner_date_of_birth',
        ];
    }

}
