<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePlannerCalculatorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('planner_calculators', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('planner_id');
            $table
                ->foreign('planner_id')
                ->references('id')
                ->on('planners')
                ->onDelete('CASCADE');
            $table->string('type');
            $table
                ->text('life_cover')
                ->nullable(true);
            $table
                ->text('trauma_cover')
                ->nullable(true);
            $table
                ->text('total_and_permanent_disablement')
                ->nullable(true);
            $table
                ->text('mortgage_repayment_and_income_protection')
                ->nullable(true);
            $table
                ->text('mortgage_repayment_income_protection_and_hec')
                ->nullable(true);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('planner_calculators');
    }
}
