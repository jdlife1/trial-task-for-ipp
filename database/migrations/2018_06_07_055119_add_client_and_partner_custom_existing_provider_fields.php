<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddClientAndPartnerCustomExistingProviderFields extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('planners', function (Blueprint $table) {
            $table
                ->text('client_custom_insurance_providers')
                ->after('client_insurance_providers')
                ->nullable(true);
            $table
                ->text('partner_custom_insurance_providers')
                ->after('client_insurance_providers')
                ->nullable(true);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('planners', function (Blueprint $table) {
            $table->dropColumn(['client_custom_insurance_providers', 'partner_custom_insurance_providers']);
        });
    }
}
