<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddHazardousFieldsOnPlanersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('planners', function (Blueprint $table) {
            $table->string('client_have_any_hazardous_activity')
                ->after('client_have_been_to_doctor_conditions')
                ->default('no')
                ->nullable(true);

            $table
                ->string('client_hazardous_activity')
                ->after('client_have_any_hazardous_activity')
                ->nullable(true);

            $table->string('partner_have_any_hazardous_activity')
                ->after('partner_have_been_to_doctor_conditions')
                ->default('no')
                ->nullable(true);

            $table->string('partner_hazardous_activity')
                ->after('partner_have_any_hazardous_activity')
                ->nullable(true);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('planners', function (Blueprint $table) {
            $table->dropColumn([
                'client_have_any_hazardous_activity',
                'client_hazardous_activity',
                'partner_have_any_hazardous_activity',
                'partner_hazardous_activity',
            ]);
        });
    }
}
