 <?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePlannerBusinessDebtsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('planner_business_debts', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('planner_id');
            $table
                ->foreign('planner_id')
                ->references('id')
                ->on('planners')
                ->onDelete('CASCADE');
            $table
                ->string('debt')
                ->nullable(true);
            $table
                ->string('personal_guarantees_in_place')
                ->nullable(true)
                ->default('no');
            $table
                ->string('personal_guarantee_value')
                ->nullable(true);
            $table
                ->string('any_long_term_contracts')
                ->nullable(true)
                ->default('no');
            $table
                ->text('contract_categories')
                ->nullable(true);
            $table
                ->text('notes')
                ->nullable(true);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('planner_business_debts');
    }
}
