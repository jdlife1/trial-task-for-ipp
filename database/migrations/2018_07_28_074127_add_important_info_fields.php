<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddImportantInfoFields extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('planners', function (Blueprint $table) {
            $table
                ->string('client_smoked_in_last_twelve_months')
                ->after('client_hazardous_activity')
                ->default('no')
                ->nullable(true);
            $table
                ->string('client_citizen_or_pr')
                ->after('client_smoked_in_last_twelve_months')
                ->default('no')
                ->nullable(true);
            $table
                ->string('client_student_or_work')
                ->after('client_citizen_or_pr')
                ->default('no')
                ->nullable(true);
            $table
                ->string('client_work_visa_greater_two_years')
                ->after('client_student_or_work')
                ->default('no')
                ->nullable(true);

            $table
                ->string('partner_smoked_in_last_twelve_months')
                ->after('partner_hazardous_activity')
                ->default('no')
                ->nullable(true);
            $table
                ->string('partner_citizen_or_pr')
                ->after('partner_smoked_in_last_twelve_months')
                ->default('no')
                ->nullable(true);
            $table
                ->string('partner_student_or_work')
                ->after('partner_citizen_or_pr')
                ->default('no')
                ->nullable(true);
            $table
                ->string('partner_work_visa_greater_two_years')
                ->after('partner_student_or_work')
                ->default('no')
                ->nullable(true);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('planners', function (Blueprint $table) {
            $table->dropColumn([
                'client_smoked_in_last_twelve_months',
                'client_citizen_or_pr',
                'client_student_or_work',
                'client_work_visa_greater_two_years',
                'partner_smoked_in_last_twelve_months',
                'partner_citizen_or_pr',
                'partner_student_or_work',
                'partner_work_visa_greater_two_years',
            ]);
        });
    }
}
