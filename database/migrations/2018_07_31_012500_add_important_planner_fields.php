<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddImportantPlannerFields extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('planners', function (Blueprint $table) {
                $table
                    ->string('client_passive_income')
                    ->after('client_has_attorney')
                    ->default('no')
                    ->nullable(true);
                $table
                    ->string('client_passive_amount')
                    ->after('client_passive_income')
                    ->default('no')
                    ->nullable(true);
                $table
                    ->string('partner_passive_income')
                    ->after('partner_has_attorney')
                    ->default('no')
                    ->nullable(true);
                $table
                    ->string('partner_passive_amount')
                    ->after('partner_passive_income')
                    ->defaul('no')
                    ->nullable(true);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('planners', function (Blueprint $table) {
            //
            $table->dropColumn([
                'client_passive_income',
                'client_passive_amount',
                'partner_passive_income',
                'partner_passive_amount',
            ]);
        });
    }
}
