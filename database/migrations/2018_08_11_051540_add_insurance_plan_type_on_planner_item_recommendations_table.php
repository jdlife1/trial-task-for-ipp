<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddInsurancePlanTypeOnPlannerItemRecommendationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('planner_item_recommendations', function (Blueprint $table) {
            $table
                ->enum('insurance_plan_type', ['personal', 'business'])
                ->after('category')
                ->default('personal');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('planner_item_recommendations', function (Blueprint $table) {
            $table->dropColumn('insurance_plan_type');
        });
    }
}
