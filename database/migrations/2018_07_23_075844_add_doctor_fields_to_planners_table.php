<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDoctorFieldsToPlannersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('planners', function (Blueprint $table) {
            $table->string('client_have_been_to_doctor')
                ->after('client_has_attorney')
                ->default('no')
                ->nullable(true);

            $table->string('client_have_been_to_doctor_conditions')
                ->after('client_have_been_to_doctor')
                ->nullable(true);

            $table->string('partner_have_been_to_doctor')
                ->after('partner_has_attorney')
                ->default('no')
                ->nullable(true);

            $table->string('partner_have_been_to_doctor_conditions')
                ->after('partner_have_been_to_doctor')
                ->nullable(true);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('planners', function (Blueprint $table) {
            $table->dropColumn([
                'client_have_been_to_doctor',
                'partner_have_been_to_doctor',
                'client_have_been_to_doctor_conditions',
                'partner_have_been_to_doctor_conditions',
            ]);
        });
    }
}
