<?php

use Illuminate\Database\Seeder;
use Ipp\Models\Role;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Role::create([
            'name' => 'administrator'
        ]);

        Role::create([
            'name' => 'advisor'
        ]);
    }
}
