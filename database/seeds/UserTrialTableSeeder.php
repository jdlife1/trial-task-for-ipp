<?php

use Ipp\Models\{ User, UserProfile, Role };
use Laravel\Passport\Client as PassportClient;

class UserTrialTableSeeder extends DatabaseSeeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $applicant = User::create([
            'name' => 'Web Developer Applicant',
            'email' => 'user@jdlife.co.nz',
            'password' => '1234',
        ]);
        $applicant->addRoles(Role::whereName('administrator')->first());
        $applicant->profile()->create();

        \Artisan::call('passport:keys', ['--force' => true]);

        $client = (new PassportClient)->forceFill([
            'user_id' => null,
            'name' => config('app.name'),
            'secret' => 'NJAbisSblhGTqi3E2aOn9NikEeaY84fvoeNMILGM',
            'redirect' => config('app.url'),
            'personal_access_client' => false,
            'password_client' => true,
            'revoked' => false,
        ]);
        $client->save();
    }
}
