<?php

namespace Ipp\Models;

use Illuminate\Database\Eloquent\Model;

class PlannerItemRecommendation extends Model
{
    protected $fillable = [
        'name',
        'category',
        'insurance_plan_type',
        'existing',
        'proposed',
        'description',
    ];

    protected $casts = [
        'existing' => 'array',
        'proposed' => 'array',
    ];

    public function planner()
    {
        return $this->belongsTo(Planner::class);
    }

    public static function boot()
    {
        parent::boot();

        static::addGlobalScope(function ($query) {
            $query->orderBy('id', 'DESC');
        });
    }
}
