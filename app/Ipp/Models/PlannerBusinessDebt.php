<?php

namespace Ipp\Models;

use Illuminate\Database\Eloquent\Model;

class PlannerBusinessDebt extends Model
{
    protected $fillable = [
        'any_long_term_contracts',
        'debt',
        'notes',
        'personal_guarantee_value',
        'personal_guarantees_in_place',
        'contract_categories',
    ];

    protected $casts = [
        'contract_categories' => 'array',
    ];

    public function planner()
    {
        $this->belongsTo(Planner::class);
    }
}
