<?php

namespace Ipp\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Planner extends Model
{
    protected $fillable = [
        'employment_position_type',
        'employed_schedule_type',
        'employed_annual_income',
        'employed_occupation',
        'employed_how_long',
        'company_full_name',
        'client_full_name',
        'business_partners',
        'income_tax_method',
        'on_tools',
        'has_corporate_partner',
        'classification_unit',
        'income_from_business',
        'nominated_cover_amount',
        'when_business_started',
        'how_many_boys',
        'what_you_do_in_business',
        'acc_number_business',
        'acc_or_ird_number_personal',
        'partner_acc_or_ird_number_personal',
        'accountants_name',
        'firm_name',
        'phone_number',
        'client_date_of_birth',
        'client_address',
        'client_mobile_number',
        'client_email_address',
        'client_smoking_status',
        'client_insurance_providers',
        'client_custom_insurance_providers',
        'client_has_will_in_place',
        'client_has_attorney',
        'client_passive_income',
        'client_passive_amount',
        'client_have_been_to_doctor',
        'client_have_been_to_doctor_conditions',
        'client_have_any_hazardous_activity',
        'client_hazardous_activity',
        'client_smoked_in_last_twelve_months',
        'client_citizen_or_pr',
        'client_student_or_work',
        'client_work_visa_greater_two_years',
        'partner_date_of_birth',
        'partner_address',
        'partner_mobile_number',
        'partner_email_address',
        'partner_smoking_status',
        'partner_insurance_providers',
        'partner_custom_insurance_providers',
        'partner_has_will_in_place',
        'partner_has_attorney',
        'partner_passive_income',
        'partner_passive_amount',
        'partner_have_been_to_doctor',
        'partner_have_been_to_doctor_conditions',
        'partner_have_any_hazardous_activity',
        'partner_hazardous_activity',
        'partner_smoked_in_last_twelve_months',
        'partner_citizen_or_pr',
        'partner_student_or_work',
        'partner_work_visa_greater_two_years',
        'have_childrens',
        'living_expenses',
        'household_building_expense',
        'mortgage_repayments',
        'mortgage_repayments_type',
        'total_debts',
        'notes',
        'estate_notes',
        'assets_notes',
        'is_partner_shareholder_or_directory',
        'partner_name',
        'is_partner_account_tax_splitting',
        'partner_income_tax_method',
        'partner_taking_from_the_firm',
        'partner_on_tools',
        'partner_classification_unit',
        'partner_nominated_cover_amount',
        'key_persons',
        'buy_and_sells',
        'partner_when_business_started',
        'partner_annual_income',
        'partner_is_working_for_business',
        'partner_is_working',
        'partner_employment_position_type',
        'partner_what_job',
        'income_protection_covers_with_other_insurance_provider',
        'passive_income_from_business',
        'passive_income_from_rentals',
        'other_passive_income',
        'client_tax_issues_insurance_provider',
        'partner_tax_issues_insurance_provider',
        'fatal_entitlement_for_type',
        'fatal_entitlement_category_type',
        'fatal_entitlement_annual_pre_aggreed_cover_amount',
        'fatal_entitlement_childrens',
        'client_signature',
        'partner_signature',
    ];

    protected $casts = [
        'key_persons' => 'array',
        'buy_and_sells' => 'array',
        'business_partners' => 'array',
        'classification_unit' => 'integer',
        'what_you_do_in_business' => 'integer',
        'client_insurance_providers' => 'array',
        'partner_insurance_providers' => 'array',
        'fatal_entitlement_childrens' => 'array',
        'partner_classification_unit' => 'integer',
        'client_custom_insurance_providers' => 'array',
        'partner_custom_insurance_providers' => 'array',
    ];

    protected $appends = ['preview_document_url'];

    protected $with = [
        'reports',
        'calculators',
        'businessDebt',
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function calculators()
    {
        return $this->hasMany(PlannerCalculator::class);
    }

    public function businessDebt()
    {
        return $this->hasOne(PlannerBusinessDebt::class);
    }

    public function getMutatedDateTime($value)
    {
        return Carbon::createFromTimestamp(strtotime($this->attributes[$value]));
    }

    public function getPreviewDocumentUrlAttribute()
    {
        return null;
    }

    public function reports()
    {
        return $this->hasMany(PlannerItemRecommendation::class);
    }
}
