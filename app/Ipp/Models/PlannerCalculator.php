<?php

namespace Ipp\Models;

use Illuminate\Database\Eloquent\Model;

class PlannerCalculator extends Model
{
    protected $fillable = [
        'type',
        'tpd',
        'life_cover',
        'trauma_cover',
        'total_and_permanent_disablement',
        'mortgage_repayment_and_income_protection',
        'mortgage_repayment_income_protection_and_hec',
        'tax_calculator',
    ];

    protected $casts = [
        'tpd' => 'array',
        'life_cover' => 'array',
        'trauma_cover' => 'array',
        'total_and_permanent_disablement' => 'array',
        'mortgage_repayment_and_income_protection' => 'array',
        'mortgage_repayment_income_protection_and_hec' => 'array',
    ];

    public function planner()
    {
        return $this->belongsTo(Planner::class);
    }
}
