<?php

namespace Ipp\Models;

use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable, HasApiTokens;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $with = [
        'roles', 'profile',
    ];

    public function setPasswordAttribute($value)
    {
        $this->attributes['password'] = bcrypt($value);
    }

    public function planners()
    {
        return $this->hasMany(Planner::class);
    }

    public function roles()
    {
        return $this->belongsToMany(Role::class);
    }

    public function profile()
    {
        return $this->hasOne(UserProfile::class);
    }

    public function addRoles($roles)
    {
        if (is_array($roles)) {
            return $this->roles()->sync($roles);
        }

        return $this->roles()->attach($roles);
    }

    public function authorizeRoles($roles)
    {
        if (is_array($roles))
          return $this->hasAnyRole($roles) ||
                 abort(401, 'This action is unauthorized.');

        return $this->hasRole($roles) ||
             abort(401, 'This action is unauthorized.');
    }

    /**
    * Check multiple roles.
    *
    * @param array $roles
    */
    public function hasAnyRole(array $roles)
    {
        return null !== $this->roles()->whereIn('name', $roles)->first();
    }
    /**
    * Check one role.
    *
    * @param string $role
    */
    public function hasRole($role)
    {
        return null !== $this->roles()->where('name', $role)->first();
    }
}
