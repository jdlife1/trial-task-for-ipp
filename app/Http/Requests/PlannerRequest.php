<?php

namespace App\Http\Requests;

use Auth;
use Illuminate\Foundation\Http\FormRequest;

class PlannerRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $inputs = $this->all();

        $rules = [
            'income_from_business' => [
                // 'required_unless:employment_position_type,employed'
            ],
            'nominated_cover_amount' => [
                // 'required_unless:employment_position_type,employed'
            ],
            'business_debt' => [
            ],
        ];

        $this->replace($inputs);

        return $rules;
    }
}
