<?php

namespace App\Http\Controllers\Api;

use Ipp\Models\Planner;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\PlannerRequest;
use App\Http\Resources\PlannerResource;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class PlannerController extends Controller
{
    public function persist(PlannerRequest $request)
    {
        $model = $request
                    ->user()
                    ->planners()
                    ->updateOrCreate($request->only('id'), $request->except(['id', 'user_id']));

        if (isset($request->business_debt)) {
            $model
                ->businessDebt()
                ->updateOrCreate(['planner_id' => $model->id], $request->business_debt);
        }

        return (new PlannerResource($model->fresh()));
    }

    public function curate(Request $request)
    {
        $models = [];
        $user = $request->user();

        if ($user->hasRole('advisor'))
            $models = $user->planners()->latest()->get();
        else
            $models = Planner::latest()->get();

        return PlannerResource::collection($models);
    }
}
