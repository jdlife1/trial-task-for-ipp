<?php

namespace App\Http\Controllers\Api;

use Ipp\Models\Planner;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\PlannerItemRecommendationRequest;
use App\Http\Resources\PlannerItemRecommendationResource;

class PlannerItemRecommendationController extends Controller
{
    public function persist(Planner $planner, PlannerItemRecommendationRequest $request)
    {
        $planner_id = $planner->id;

        $name = $request->get('name');

        $planner->reports()->create($request->all());

        return (new PlannerItemRecommendationResource($planner->fresh()));
    }
}
