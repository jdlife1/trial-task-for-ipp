# IPP (Trial)

[![N|Solid](https://jdlife.co.nz//assets/images/jdlife/logo.png)](https://jdlife.co.nz)

### Tech

IPP uses a number of open source projects to work properly:

* [VueJs](http://vuejs.org) - UI Framework.
* [Laravel](https://laravel.com/docs/5.6/) - Backend.
* [Quasar](https://v0-16.quasar-framework.org/) - Empowering your websites and apps.

### Laravel Installation and Configuration
IPP requires [PHP](http://php.net/) v7.1.16+ and must not be 7.2+ to run.
```sh
$ composer install
```
Initial Configuration
```sh
$ cp .env.example .env
$ php artisan key:generate
```

> See .env line 48
> WEB_ENDPOINT=http://your-laravel-url-endpoint.localhost

After Database Configuration
```sh
$ php artisan migrate
$ php artisan db:seed
$ php artisan serve
```

### VueJs Installation
IPP requires [Node.js](https://nodejs.org/) >= 8.9.0 is required. to run.

> Quasar Cli [installation guide](https://v0-16.quasar-framework.org/guide/quasar-cli.html)

Install the dependencies and devDependencies and start the server.

```sh
$ cd /your-repository
$ npm install
$ quasar dev
```
